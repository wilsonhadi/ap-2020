package id.ac.ui.cs.tutorial5.core;

import id.ac.ui.cs.tutorial5.service.RandomizerService;

public class BirdEggs extends Craftable {

    public BirdEggs() {
        super("Birds Egg", RandomizerService.getRandomCostValue());
    }

    @Override
    public String getDoneCraftMessage() {
        return "From the egg, a cute little bird is born \n";
    }
}
