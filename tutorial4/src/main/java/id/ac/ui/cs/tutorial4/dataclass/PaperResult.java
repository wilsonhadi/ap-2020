package id.ac.ui.cs.tutorial4.dataclass;

import java.util.ArrayList;
import java.util.List;

public class PaperResult {
    private List<Paper> paperList = new ArrayList<>();
    private boolean isError;

    public List<Paper> getPaperList() {
        return paperList;
    }

    public void setPaperList(List<Paper> paperList) {
        this.paperList = paperList;
    }


    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }
}
