package id.ac.ui.cs.advprog.tutorial7.strategy.repository;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.AttackAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.DefenseAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.SupportAction;

import java.util.List;
import java.util.ArrayList;

import org.springframework.stereotype.Repository;

@Repository
public class ActionRepository {
    private List<AttackAction> attackList = new ArrayList<>();
    private List<DefenseAction> defenseList = new ArrayList<>();
    private List<SupportAction> supportList = new ArrayList<>();

    public List<AttackAction> getAttackActions(){
        return attackList;
    }

    public List<DefenseAction> getDefenseActions(){
        return defenseList;
    }

    public List<SupportAction> getSupportActions(){
        return supportList;
    }

    public AttackAction addAttackAction(AttackAction param){
        attackList.add(param);
        return param;
    }

    public DefenseAction addDefenseAction(DefenseAction param){
        defenseList.add(param);
        return param;
    }

    public SupportAction addSupportAction(SupportAction param){
        supportList.add(param);
        return param;
    }
}