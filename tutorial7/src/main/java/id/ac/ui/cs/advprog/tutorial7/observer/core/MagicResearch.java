package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class MagicResearch {
        private String title;
        private String type;

        public void setTitle(String title) {
                this.title = title;
        }

        public void setType(String type) {
                this.type = type;
        }

        public String getTitle() {
                return this.title;
        }

        public String getType() { return this.type; }
}
