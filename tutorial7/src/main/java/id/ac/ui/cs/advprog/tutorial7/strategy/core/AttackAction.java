package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public interface AttackAction extends Action {
	
	String attack();
	
}