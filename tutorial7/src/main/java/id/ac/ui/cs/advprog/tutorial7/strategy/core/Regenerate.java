package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Regenerate implements SupportAction {
	
	private static final String ACTION_NAME = "Regenerate";
	
	public String getDescription(){
		return "A spell that let targeted magician to heal over time";
	}
	
	public String support(){
		return "Activate " + ACTION_NAME + ". Targeted magician will heal over time";
	}
}