package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Barrier implements DefenseAction {
	
	private static final String ACTION_NAME = "Barrier";
	
	public String getDescription(){
		return "A spell used to reduce offensive spell's power";
	}
	
	public String defense(){
		return "Activate " + ACTION_NAME + ". The magician will receive less damage";
	}
}