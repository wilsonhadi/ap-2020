package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Quest {
        //Todo: Some variables are publicly accessed
        //Todid: change to private
        private String title;
        private String type;

        public void setTitle(String title) {
                this.title = title;
        }

        public void setType(String type) {
                this.type = type;
        }

        public String getTitle() {
                return this.title;
        }

        public String getType() { return this.type; }
}
